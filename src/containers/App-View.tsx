import ItemList from '@/components/appdetails/ItemList'
import appdata from '@/constants/app-data.json'
const AppView=()=>{
   const itemdata=appdata.properties.models.map(model=>({id:model.id,name:model.name}))
   return (
    <>
    <ItemList data={itemdata} search={false} Type='List' />
    </>
   )
}
export default AppView
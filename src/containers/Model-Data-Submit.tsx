import React from 'react';
import { useRouter } from 'next/router';
import Customview from '@/components/customdataview/Customview';
const ViewData = () => {
    const router = useRouter();
    const { Locationname, LocationType, Latitude, Longitude } = router.query;
    if(router.query){
        console.log(router.query)
    }
    return (
        <div className='w-full mt-4 h-3/4'>
            <h1 className='relative text-xl font-semibold text-left top-12 left-12'>View Data</h1>
            <Customview name={Locationname} Type={LocationType} Latitude={Latitude} Longitude={Longitude} />
        </div>
    );
};

export default ViewData;

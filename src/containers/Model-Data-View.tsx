import modeldata from '@/constants/model-data.json'
import ItemList from '@/components/model/itemList'
const ModelView=()=>{
    const  itemList=modeldata.map(model=>({Key:model.key, SNO:model.SNO,Locationname:model.Locationname,LocationType:model.LocationType,Latitude:model.Latitude,Longitude:model.Longitude}))
    return (
        <ItemList initialData={itemList} search={true} Type="Table" filter={true}/>
    )
}
export default ModelView

import * as React from 'react';
import AppQuickview from './App-Quick-View';
import data from '@/utils/Data'
import Appdata from '@/constants/app_model_data.json'
import ItemList from '@/components/manageapps/ItemList';
const ManageAppsView: React.FC = () => {
  const [chartType, setChartType] = React.useState<string>('');
  const Data = Appdata.apps.map(app => ({ id: app.id, name: app.name }));
  const changeChartType = (selectedChartType: string) => {
    setChartType(selectedChartType);
  };
  return (
    <>
      <div className="mt-2 flex space-x-6 ">
        <ItemList Data={Data} onItemClick={changeChartType} search={true} Type='card' />
        <div className="flex flex-col space-y-2 mt-6">
          <h1 className='text-xl font-bold text-left'>Analysis</h1>
          <div className="flex space-x-8">
            <select
              className='mt-2 ml-2 p-2 rounded-lg px-8 py-12 text-left'
              value={chartType}
              onChange={(e) => changeChartType(e.target.value)}
            >
              <option value="">Choose</option>
              <option value="pie">pie</option>
              <option value="bar">bar</option>
              <option value="garph view">Graph</option>
              <option value="list view">list</option>

            </select>
          </div>
          <AppQuickview chartType={chartType} data={data} />
        </div>
      </div>
    </>
  );
};

export default ManageAppsView;

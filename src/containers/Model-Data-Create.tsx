import * as React from 'react'
import Back from '@/components/Back'
import Input from '@/components/model/Input'
import { useRouter } from 'next/router'
const ModelDataCreate:React.FC = () => {
    const router=useRouter()
    const {create}=router.query
    return (
        <>
             <Back link={`/handleViewData/modelData/${create}/view`}/>
            <h1 className='mt-8 text-left ml-5 text-2xl font-bold'>Add a Location</h1>
            <div className="flex space-x-2 bg-gray-400 w-full  rounded-lg h-[80%] mt-4">
              <Input/>
            </div>
        </>
    )
}
export default ModelDataCreate;
import Chart from 'chart.js/auto';
const renderChart = (chartInstance: React.MutableRefObject<Chart | null>, ctx: any, Chartdata: Data, chartType: any) => {
    if (chartInstance.current) {
        chartInstance.current.destroy();
        chartInstance.current = null;
    }
    if (chartType !== "" && Chartdata) {
        chartInstance.current = new Chart(ctx, {
            type: chartType,
            data: Chartdata,
        });
    }
};

export { renderChart };

import * as React from 'react'
import { useSearchParams } from 'next/navigation'
import Actions from '@/components/appview/Actions';
import { renderChart } from './Analysis';
import Chart from 'chart.js/auto';
interface Props {
  chartType: any;
  data: Data
}
const AppQuickView: React.FC<Props> = ({ chartType, data }): any => {
  const appparams = useSearchParams()
  const appid = appparams.get('id')
  const chartRef = React.useRef<HTMLCanvasElement>(null);
  const chartInstance = React.useRef<Chart | null>(null);
  const prepareChartData = () => {
    const Chartdata = {
      labels: data.labels,
      datasets: [
        {
          label: data.datasets[0].label,
          data: data.datasets[0].data,
          backgroundColor: data.datasets[0].backgroundColor,
          borderWidth: data.datasets[0].borderWidth,
        },
      ],
    };

    return { Chartdata };
  }
  React.useEffect(() => {
    const ctx = chartRef.current?.getContext('2d');
    const { Chartdata } = prepareChartData();
    renderChart(chartInstance, ctx, Chartdata, chartType);
  }, [chartType, data, appid]);
  return (
    <>
      <div style={{ boxSizing: 'border-box', height: '386px', width: '450px', paddingTop: '36px' }}>
        <canvas ref={chartRef}></canvas>
      </div>
      {chartType && <Actions chartType={chartType} />}
    </>
  )
};

export default AppQuickView
import { useReducer, createContext } from "react";
import data from '@/constants/model-data.json';
enum FormActionTypes {
    ADD_Data = 'AddData'
}
type FormAction = { type: FormActionTypes.ADD_Data; payload: any };
interface ActionCreators {
    addFormData: (data: any[]) => void;
}
export const FormContext = createContext<{
    state: FormState[];
    dispatch: React.Dispatch<FormAction>;
    addFormData: ActionCreators['addFormData'];
}>({
    state: [],
    dispatch: () => {},
    addFormData: () => {}
});
const addFormData = (state: FormState[], newdata: any[]) => {
    return [
        ...state,
        ...newdata
    ];
}
const FormReducer = (state: FormState[], action: FormAction) => {
    switch(action.type) {
        case FormActionTypes.ADD_Data:
            return addFormData(state, action.payload);
        default:
            return state;
    }
}
const FormProvider = ({ children }: { children: React.ReactNode }) => {
    const [state, dispatch] = useReducer(FormReducer, data);
    const addFormDataAction = (newData: any[]) => {
        dispatch({ type: FormActionTypes.ADD_Data, payload: newData });
    }
    return (
        <FormContext.Provider value={{ state, dispatch, addFormData: addFormDataAction }}>
            {children}
        </FormContext.Provider>
    );
}
export default FormProvider;

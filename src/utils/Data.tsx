const data: Data = {
  labels: ['Network-Activity', 'Storage Collection', 'CPU', 'Battery Usage'],
  datasets: [
    {
      label: 'system statistics',
      data: [17, 26, 26, 30],
      backgroundColor: [
        '#3498db',
        '#2ecc71',
        '#9b59b6',
        '#f1c40f',
      ],
      borderWidth: 1
    }
  ]
};

export default data;

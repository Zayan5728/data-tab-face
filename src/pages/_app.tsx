import '@/styles/globals.css'
import type { AppProps } from "next/app";
import Layout from "@/components/layout/Layout";
import FormProvider from '@/context/FormProvider';
export default function App({ Component, pageProps }: AppProps) {
  return <Layout>
    <FormProvider>
    <Component {...pageProps} />;
    </FormProvider>
    </Layout>
}





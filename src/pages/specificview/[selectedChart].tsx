import React from 'react'
import { useRouter } from 'next/router'
import data from '@/utils/Data';
import Operations from '@/components/Operations';
import AppDisplay from '@/containers/App-Quick-View';
function index() {
  const router=useRouter()
  console.log(router.query)
  const {selectedChart}=router.query
  return (
    <>
       <h1 className='font-semibold  text-left'>Dashboard view</h1>
       <div className="flex space-x-6">
        <div className=" w-[500px] h-[386px]  mt-4 bg-gray-200
        rounded-lg">
            <AppDisplay chartType={selectedChart} data={data as Data} />
            <Operations/>
        </div>
       </div>
    </>
  )
}

export default index
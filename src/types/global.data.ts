export { }
declare global {
    interface Data {
        labels: string[];
        datasets: {
            label: string;
            data: number[];
            backgroundColor: string[];
            borderWidth: number;
        }[];
    }
    interface FormState {
        key: number,
        SNO: number,
        Locationname: string;
        LocationType: string;
        Latitude: number;
        Longitude: number;
    }
}
interface Props{
    options:string[]
}
const Filter:React.FC<Props>=({options})=>{
    return (
        <>
          <select
                className='mt-4  w-30 h-9 p-2 rounded-lg relative text-lg text-black'
            >
                <option className="flex space-x-2">Filter</option>
                {options.map(option=><option key={option}>
                    {option}
                </option>)}
            </select>
        </>
    )
}
export default Filter
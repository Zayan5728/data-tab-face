import * as React from 'react'
import Image from 'next/image'
import Link from 'next/link'
import { useSearchParams } from 'next/navigation'
import Back from '../Back'
interface Props {
  data: {
    id: number,
    name: string
  }[]
  search: boolean,
  Type: String
}
const ItemList: React.FC<Props> = ({ data, search, Type }) => {
  const idParams = useSearchParams()
  const id = idParams.get('id')
  return (
    <>
      {!search && <>
        <Back link={`./apps?id=${id}`} />
        <h1 className='text-2xl font-bold mt-2 ml-2 text-left'>GIS App</h1>
      </>}
      {Type=='List' &&  <div className="models mt-4 ml-2 bg-slate-600 rounded-lg  w-[572px] h-[734px] flex-col space-y-4">
        {data && data.map(data =>
          <div className="first one flex justify-between items-center p-2">
            <h1 className='font-jost text-2xl font-semibold text-white ml-4'>{data.name}</h1>
            <div className="flex space-x-8 items-center mr-4">
              <Link href={`/handleViewData/modelData/${data.id}/view`}><Image src={'/assets/view-filled.png'} alt='view' width="27" height="27" /></Link>
              <Image src={'/assets/Group Add.png'} alt='view' width="24" height="24" />
            </div>
          </div>)}
      </div>
      }
    </>
  )
}
export default ItemList
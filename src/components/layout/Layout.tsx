import * as React from 'react'
import MainNavigation from './MainNavigation';
import Image from 'next/image';
import Head from 'next/head';
interface Props {
    children: React.ReactNode;
}
export const metadata = {
    title: 'Next-JS',
    description: 'Data tab face'
}
const Layout: React.FC<Props> = ({ children }) => {
    return (
        <div>
            <Head>
                <title>{metadata.title}</title>
                <meta name="description" content={metadata.description} />
            </Head>
            <div className="icons flex justify-between items-center mt-2">
                <div className="text-black font-jost text-2xl font-medium">
                    114 AI
                </div>
                <div className="flex space-x-8 items-center">
                <Image src={'/assets/bell.png'} alt='bell' width="42" height="42"/>
                <Image src={'/assets/settings.png'} alt='settings' width="42" height="42"/>
                </div>
            </div>

            <div className="flex flex-row space-x-2">
                <MainNavigation />
                <main className='w-[1725px] h-[937px] text-center p-4 border-r-8 rounded-lg bg-gray-300'>
                    {children}
                </main>
            </div>
        </div>
    );
};

export default Layout;


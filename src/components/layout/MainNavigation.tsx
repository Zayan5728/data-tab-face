import Link from 'next/link';
import * as React from 'react'
import Image from 'next/image';
function MainNavigation() {
    return (
        <div className="">
            <nav className="flex flex-col p-4 w-24 space-y-64 border-r-8 bg-purple-200 rounded-lg h-[937px]">
                <div className="flex flex-col space-y-12">
                    <Link href='/' className='w-9 h-9'>
                      <Image alt='Home' src={'/assets/Vector.png'} width="38" height="36"/>
                    </Link>
                    <Link href='/apps' className='w-9 h-9'>
                    <Image alt='Home' src={'/assets/dashboard.png'} width="38" height="36"/>
                    </Link>
                    <Link href='/teeny' className='w-9 h-9'>
                    <Image alt='Home' src={'/assets/teenyicon.png'} width="38" height="36"/>
                    </Link>
                </div>
                <div className="flex flex-col space-y-8">
                    <Link href='/Account' className='w-9 h-9'>
                    <Image alt='Home' src={'/assets/Account.png'} width="38" height="36"/>
                    </Link>
                    <Link href='/Back' className='w-9 h-9 ml-2'>
                    <Image alt='Home' src={'/assets/Group 365.png'} width="38" height="36"/>
                    </Link>
                </div>
            </nav>
        </div>
    )
}

export default MainNavigation;

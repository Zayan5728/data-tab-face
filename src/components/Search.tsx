import * as React from 'react';
const Search: React.FC = () => {
  // const Change=(e:any)=>{
  //   handlesearch(e.target.value)
  // }
  return (
    <div className="search ml-1 flex space-x-6  relative">
      <input type="text" placeholder='search' className='text-gray-400 p-2 pl-8 border-r-2 rounded-lg' />
    </div>
  );
}

export default Search;

import React, { useEffect, useState } from "react";
import Link from "next/link";
import data from "@/utils/Data"
import { useSearchParams } from 'next/navigation'
import Filter from '@/components/filter/FilterBy'
import AppDisplay from "@/containers/App-Quick-View";
type ChartType = string;
const DashboardViews = () => {
    const [chartTypes, setChartTypes] = useState<ChartType[]>([]);
    const titleparams = useSearchParams()
    const title = titleparams.get('id')
    const filterby=[
        'Today','Yesterday','Last 7days','Last 30days'
      ]
    useEffect(() => {
        if (typeof window !== 'undefined' && title) {
            const chartTypesFromLocalStorage = localStorage.getItem(`chartTypes${title}`);
            if (chartTypesFromLocalStorage !== null) {
                const parsedChartTypes: ChartType[] = JSON.parse(chartTypesFromLocalStorage);
                setChartTypes(parsedChartTypes);
            }
        }
    }, [title]); 
    return (
        <> 
        <div className="flex justify-between">
            <h1 className="text-xl text-left font-semibold">Dashboard view</h1>
            <button className="text-white px-8 py-2 bg-black rounded mr-2 text-lg">Refresh</button>
        </div>
        <div className="text-left">
            <Filter options={filterby}/>
        </div>
            <div className="grid grid-cols-3 mt-4 space-x-10 bg-gray-200 border border-r-0">
                {chartTypes.map((chartType, index) => (
                    <Link href={`./specificview/${chartTypes[index]}`} >
                        <div className="flex space-x-6 cursor-pointer">
                            <div className="pl-5 w-[500px] h-[386px] bg-gray-400 rounded-lg" key={index}>
                                <AppDisplay chartType={chartType} data={data as Data} />
                                <h2 className="relative text-white font-semibold left-40 bottom-6">{new Date().getMinutes()} Minutes ago</h2>
                            </div>
                        </div>
                    </Link>
                ))}
            </div>
        </>
    );
}

export default DashboardViews;

import * as React from 'react';
import Appdata from '@/constants/app_model_data.json'
import { useRouter } from 'next/router';
import Search from '../Search';
interface Props {
    Data: {
        id: number;
        name: string;
    }[]
    onItemClick: (selectedChartType: string) => void;
    search: boolean,
    Type:string
}
const ItemList: React.FC<Props> = ({ Data, onItemClick, search,Type }) => {
    const router = useRouter()
    const handleButtonClick = (id: number) => {
        onItemClick("pie");
        router.push({
            pathname: '/apps',
            query: { id: id },
        })
    };
    // const[searchquery,setSearchquery]=React.useState('')
    // const[filterquery,setFilterQuery]=React.useState('')
    // const handlesearch=(newsearchquery:any)=>{
    //        setSearchquery(newsearchquery)
    //        Data.map(data=>{
    //         if(data.name.includes(searchquery)){
    //             setFilterQuery(data.name)
    //         }
    //        })
    // }
    return (
        <div className="cards">
            {search && <Search/>}
            {Type==='card'&&  <div className="cards mt-4 w-[1060px] h-[820px] bg-gray-400 grid grid-cols-3 gap-x-2 rounded-lg border-r-0">
                {Data && Data.map(Data =>
                    <>
                        <div className="01 rounded-lg ml-2  w-[315px] h-[211px] bg-gray-500 text-white">
                            <div key={Data.id} className="ml-3 mt-2 w-[295px] h-[52px] bg-gray-600 hover:bg-gray-900">
                                <button onClick={() => handleButtonClick(Data.id)} className='text-center text-lg font-bold'>{Data.name}</button>
                            </div>
                            <div className="flex flex-col space-y-3 h-36 overflow-y-scroll text-lg font-semibold ml-2 mt-2 text-left">
                                {Appdata.apps.find(app => app.name === Data.name)?.models.map(model => (
                                    <div key={model.id}>{model.name}</div>
                                ))}
                            </div>
                        </div>
                    </>
                )}
            </div>}
        </div>
    );
};

export default ItemList;

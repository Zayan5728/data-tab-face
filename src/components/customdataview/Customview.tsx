import React from 'react'
interface Props {
    name: string | string[] | undefined,
    Type: string | string[] | undefined,
    Latitude: string | string[] | undefined
    Longitude: string | string[] | undefined
}
const Customview: React.FC<Props> = ({name,Type,Latitude,Longitude}) => {
    const [selectedFormat, setSelectedFormat] = React.useState<string>('');
    const handleFormatChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
        setSelectedFormat(e.target.value);
    };
    const renderData = () => {
        switch (selectedFormat) {
            case 'Int':
                return (
                    <>
                        <p>Location Name: {name}</p>
                        <p>Location Type: {Type}</p>
                        <p>Latitude: {Latitude}</p>
                        <p>Longitude: {Longitude}</p>
                    </>
                );
            case 'Character String':
                return (
                    <>
                        <p>Location Name: {name}</p>
                        <p>Location Type: {Type}</p>
                        <p>Latitude: {Latitude}</p>
                        <p>Longitude: {Longitude}</p>
                    </>
                );
            case 'Json':
                return <pre>{JSON.stringify({ name, Type, Latitude, Longitude }, null, 2)}</pre>;
            case 'Txt':
                return (
                    <>
                        <p>Location Name: {name}</p>
                        <p>Location Type: {Type}</p>
                        <p>Latitude: {Latitude}</p>
                        <p>Longitude: {Longitude}</p>
                    </>
                );
            default:
                return null;
        }
    };
    return (
        <>
            <div className="flex space-x-6">
                <h2 className='relative text-xl font-semibold text-left left-16'>Schema Type:</h2>
                <select
                    className='relative left-16 w-40 h-10 p-2 rounded-lg'
                    value={selectedFormat}
                    onChange={handleFormatChange}
                >
                    <option value="">Choose</option>
                    <option value="Int">Int</option>
                    <option value="Character String">Character String</option>
                    <option value="Json">Json</option>
                    <option value="Txt">Txt</option>
                </select>
            </div>
            <div className="relative text-xl mt-4 rounded-lg text-left left-48 w-1/2 p-4 bg-gray-500">
                {renderData()}
            </div>
        </>
    )
}

export default Customview
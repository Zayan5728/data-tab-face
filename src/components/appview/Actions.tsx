import * as React from 'react'
import Link from 'next/link';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { useSearchParams } from 'next/navigation';
interface Props {
  chartType: string
}
const Actions: React.FC<Props> = ({ chartType }) => {
  const [show, setShow] = React.useState(false);
  const router = useRouter();
  const appparams = useSearchParams()
  const appId =appparams.get('id')
  const handle = ():void => {
    setShow(!show);
    if (appId) {
      const storedChartTypesJson = localStorage.getItem(`chartTypes${appId}`);
      const storedChartTypes: string[] = storedChartTypesJson ? JSON.parse(storedChartTypesJson) : [];
      if (!storedChartTypes.includes(chartType)) {
        storedChartTypes.push(chartType);
      }
      localStorage.setItem(`chartTypes${appId}`, JSON.stringify(storedChartTypes));
    }
  };
  React.useEffect(() => {
    const timeoutId = setTimeout(() => {
      if (show) {
        router.push({
          pathname:'/dashboardview',
          query:{id:appId},
         
        },
        )
      }
    }, 5000);
    return () => clearTimeout(timeoutId);
  }, [show]);
  return (
    <>
      <div className="details ml-0 pt-[13rem]">
        {show && (
          <div className="flex space-x-3 rounded-lg p-4 justify-center items-center bg-black text-white font-sans font-medium absolute top-[43rem] right-[12rem]">
            <Image src={'/assets/pin.png'} alt='view' width="25" height="25" />
            <button>Pinned to dashboard</button>
          </div>
        )}
        <div className="flex space-x-6">
          <div className="flex space-x-3 rounded-lg p-6 justify-center items-center bg-black text-white font-sans font-medium">
            <Image src={'/assets/pin.png'} alt='view' width="25" height="25" />
            <button onClick={handle}>Pin to dashboard</button>
          </div>
          <Link href={`/app/${appId}`} className="text-white font-Jost font-medium text-base bg-black rounded-lg p-4 font-sans">
            View App details
          </Link>
        </div>
      </div>
    </>
  );
}
export default Actions;

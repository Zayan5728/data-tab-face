import Image from "next/image"
import React from "react"
import SpecificView from "./SpecificView"
const Operations = ()=>{
    const [show, setShow] = React.useState<any>(false)
    const handle = () => {
        setShow((show: boolean) => !show)
    }
    return (
        <>
        <button onClick={handle} className="relative bottom-[22rem] left-48  cursor-pointer"><Image src={'/assets/Frame 380.png'} alt='frame' width='5' height='29' /></button>
        {show && <SpecificView/>}
        </>
    )
}
export default Operations
import React, { useState } from 'react';
import Link from 'next/link';
import Search from '../Search';
import Image from 'next/image';
import Filter from './Filter';
import { useRouter } from 'next/router';
import Table from './Table';
interface Props {
    initialData: {
        Key: number;
        SNO: number;
        Locationname: string;
        LocationType: string;
        Latitude: string;
        Longitude: string;
    }[];
    search: boolean;
    Type: string;
    filter: boolean;
}
const ItemList: React.FC<Props> = ({ initialData, search, Type, filter }) => {
    const router = useRouter();
    const { modelId } = router.query;
    const [showFilter, setShowFilter] = useState<boolean>(false);
    const handleClick = () => {
        setShowFilter((showFilter) => !showFilter);
    };
    return (
        <>
            <div className="time filter flex space-x-4 justify-center">
            <input
                type="range"
                min="1"
                max="500"
              />
              <button className='text-black text-lg bg-gray-500 p-3'>Apply time filter</button>
            </div>
            <div className="flex items-center mt-2 ">
                {search && <Search />}
                <div className="ml-auto flex space-x-2 relative mr-16">
                    <button className="px-10 py-2 bg-gray-400 font-semibold text-2xl flex" onClick={handleClick}>
                        Filter
                        <Image src="/assets/filter-menu.png" alt="filter" width={24} height={24} className="ml-2 py-1" />
                    </button>
                    {filter && showFilter && <Filter />}
                    <Link href={`/handleAddData/model-data/${modelId}/create`} className="px-10 py-2 flex bg-gray-500 font-semibold text-2xl">
                        Create
                        <Image src="/assets/crud.png" alt="filter" width={24} height={24} className="ml-2 py-1 bg-black" />
                    </Link>
                </div>
            </div>
            {Type === "Table" && <Table />}
        </>
    );
};

export default ItemList;

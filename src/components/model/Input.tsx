import React, { useState } from 'react';
import { useRouter } from 'next/router';
import { useContext } from 'react';
import { FormContext } from '@/context/FormProvider';
import data from '@/constants/model-data.json';
const InputForm:React.FC = () => {
    const router = useRouter();
    const { create } = router.query;
    const useFormContext=useContext(FormContext)
    const {addFormData,}=useFormContext
    const [formData, setFormData] = useState({
        Locationname: '',
        LocationType: '',
        Latitude: '',
        Longitude: ''
    });
    const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
    };
    const Key = Math.max(...data.map(item => item.key));
    const nextKey = Key + 1;
    const SNO = Math.max(...data.map(item => item.SNO));
    const nextSNO = SNO + 1;
    const handleSubmit = (e:React.FormEvent) => {
        e.preventDefault();
        const newData = [{
            ...formData,
            key:nextKey,
            SNO: nextSNO
        }];
        addFormData(newData)
        setFormData({
            Locationname: '',
            LocationType: '',
            Latitude: '',
            Longitude: ''
        })
        setTimeout(()=>{
            router.push({
                pathname: `/handleViewData/modelData/${create}/view`,
            });
        },2000)
    };
    return (
        <form onSubmit={handleSubmit} className="flex flex-col space-y-20  w-[60%] py-16 px-12">
            <div className="flex space-x-4">
                <label className='text-xl font-semibold'>Location:</label>
                <input
                    type='text'
                    name='Locationname'
                    value={formData.Locationname}
                    onChange={handleChange}
                    className='bg-white text-black w-[400px] h-[57px] rounded-lg relative left-11 p-2'
                />
            </div>
            <div className="flex space-x-4">
                <label className='text-xl font-semibold'>Location Type:</label>
                <select
                    name="LocationType"
                    value={formData.LocationType}
                    onChange={handleChange}
                    className='bg-white text-black w-[400px] h-[57px] rounded-lg relative p-2'
                >
                    <option value="">Choose Location</option>
                    <option value="Harbour/port">Harbour/port</option>
                    <option value="canal/channel">canal/channel</option>
                    <option value="Naval base">Naval base</option>
                    <option value="Launch pad">Launch pad</option>
                </select>
            </div>
            <div className="flex space-x-4">
                <label className='text-xl font-semibold'>Latitude:</label>
                <input
                    type='text'
                    name='Latitude'
                    value={formData.Latitude}
                    onChange={handleChange}
                    className='bg-white text-black w-[400px] h-[57px] rounded-lg relative left-12 p-2'
                />
            </div>
            <div className="flex space-x-4">
                <label className='text-xl font-semibold'>Longitude:</label>
                <input
                    type='text'
                    name='Longitude'
                    value={formData.Longitude}
                    onChange={handleChange}
                    className='bg-white text-black w-[400px] h-[57px] rounded-lg relative left-12 p-2'
                />
            </div>
            <button type="submit" className='bg-gray-500 text-black p-4 mt-2 w-fit rounded-lg  relative left-[87rem] top-28'>Add Data</button>
        </form>
    );
};
export default InputForm;

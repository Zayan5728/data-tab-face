import React from 'react'
const Filter: React.FC = () => {
  return (
    <div className="absolute top-16 right-10 flex flex-col space-y-4 bg-gray-200 w-[300px] p-4 rounded-lg shadow-lg text-left z-10">
      <div className="flex flex-col space-y-2">
        <h1 className='text-xl font-semibold text-black relative'>Location Name</h1>
        <input type='text' name="LocationName" className='rounded-lg p-2 w-full' />
      </div>
      <div className="flex flex-col space-y-2">
        <h1 className='text-xl text-black font-semibold relative'>Location Type</h1>
        <select
          name="LocationType"
          className='bg-white rounded-lg relative p-2'
        >
          <option value="">Choose Location</option>
          <option value="asc">ascending</option>
        </select>
      </div>
      <div className="flex flex-col space-y-2">
        <h1 className='text-xl text-black font-semibold relative'>Latitude</h1>
        <input type='text' name="Latitude" className='rounded-lg p-2 w-full' />
      </div>
      <div className="flex flex-col space-y-2">
        <h1 className='text-xl text-black font-semibold relative'>Longitude</h1>
        <input type='text' name="Longitude" className='rounded-lg p-2 w-full' />
      </div>
      <button className='p-2 w-fit bg-black text-white relative left-[10.5rem]'>Apply filter</button>
    </div>
  );
};

export default Filter;

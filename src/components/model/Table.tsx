import { useContext, useState } from 'react';
import { FormContext } from '@/context/FormProvider';
import Image from "next/image";
import { useRouter } from 'next/router'
const Table = () => {
    const useFormContext = useContext(FormContext)
    const { state } = useFormContext
    const [data, setData] = useState<FormState[]>(state)
    const router = useRouter()
    const navigate = (id: any) => {
        const data = state.find(item => item.SNO === id)
        router.push({
            pathname: '/viewdata',
            query: JSON.stringify(data)
        })
    }
    const onDelete = (id: number) => {
        setData(data.filter(item => item.SNO !== id))
    }
    return (
        <>
            <div className="mt-12 overflow-auto max-h-[706.85px] w-[90%] relative  rounded-lg border border-gray-600 bg-gray-600 text-white text-lg">
                <table className="w-full">
                    <thead>
                        <tr>
                            <th className="border border-gray-300 px-4 py-2">Key</th>
                            <th className="border border-gray-300 px-4 py-2">SNO</th>
                            <th className="border border-gray-300 px-4 py-2">Location Name</th>
                            <th className="border border-gray-300 px-4 py-2">Location Type</th>
                            <th className="border border-gray-300 px-4 py-2">Latitude</th>
                            <th className="border border-gray-300 px-4 py-2">Longitude</th>
                            <th className="border border-gray-300 px-4 py-2">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map(item => (
                            <tr key={item.key}>
                                <td className="border border-gray-300 px-4 py-2">{item.key}</td>
                                <td className="border border-gray-300 px-4 py-2">{item.SNO}</td>
                                <td className="border border-gray-300 px-4 py-2">{item.Locationname}</td>
                                <td className="border border-gray-300 px-4 py-2">{item.LocationType}</td>
                                <td className="border border-gray-300 px-4 py-2">{item.Latitude}</td>
                                <td className="border border-gray-300 px-4 py-2">{item.Longitude}</td>
                                <td className="border border-gray-300 px-4 py-2">
                                    <div className="flex flex-row space-x-2">
                                        <Image src={'/assets/solar_key-bold.png'} alt='solarkey' width="40" height="40" />
                                        <Image src={'/assets/mdi_graph-outline.png'} alt='outline' width="32" height="32" />
                                        <button onClick={() => navigate(item.SNO)}><Image src={'/assets/carbon_task-view.png'} alt='carbon' width="33" height="33" /></button>
                                        <button onClick={() => onDelete(item.SNO)}><Image src={'/assets/ic_baseline-delete.png'} alt='delete' width="37" height="37" /></button>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </>
    )
}
export default Table
import React from 'react'
import Image from 'next/image'
import Link from 'next/link'
interface Props{
  link:String
}
const Back:React.FC<Props>=({link})=>{
  return (
    <div className="back flex space-x-3 mt-2">
         <Image src={'/assets/arrow-left.png'} alt='back' width="34" height="34"/>
         <Link href={`${link}`}>
            <h1 className='text-xl font-bold'>Back</h1></Link>
         </div>
  )
}

export default Back;